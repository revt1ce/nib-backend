const express = require("express");
const mongoose = require("mongoose");
const app = express();
const path = require("path");

/* CORS CONFIG */
const cors = require("cors");
const corsOptions = {
	credentials: true,
};
app.use(cors(corsOptions));

/* APOLLO CONFIG */
const { ApolloServer } = require("apollo-server-express");
const typeDefs = require("./src/graphql/schema");
const resolvers = require("./src/graphql/resolvers/index");
const jwt = require("jsonwebtoken");
const { JWT_SECRET, DB_URL } = require("./src/utils/constants");

const server = new ApolloServer({
	typeDefs,
	resolvers,
	context: ({ req }) => {
		const token = req.headers.authorization || "";
		const language = req.headers.language || "";
		try {
			const data = jwt.verify(token, JWT_SECRET);
			return {
				user: data.user,
				language: language,
			};
		} catch (err) {
			return {
				user: false,
				language: language,
			};
		}
	},
});
server.applyMiddleware({ app });

/* MONGOOSE CONNECTION */
mongoose.connect(
	DB_URL,
	{
		useCreateIndex: true,
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	},
	(err) => {
		if (err) console.error(err);
		else console.log("Successfully Connected to Mongoose!");
	}
);

app.use("/upload", require("./src/routes/upload"));
app.use(express.static(path.join(__dirname, "public")));
/* APP STARTS */
app.listen(8081, (err) => {
	if (err) {
		console.error(err);
	} else {
		console.log(`----server started----`);
	}
});
