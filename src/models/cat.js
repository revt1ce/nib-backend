const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const imageSchema = new mongoose.Schema({
	path: String,
	width: Number,
	height: Number,
	type: String,
});
const catSchema = new Schema({
	key: String,
	name: String,
	slug: String,
	parentKey: String,
	childrenCats: [
		{	
			type: mongoose.Schema.Types.ObjectId,
			ref: "Cat",
		},
	],
	layout: Number,
	image: imageSchema,
	ordering: { type: Number, required: true, default: () => new Date().getTime() },
	language: { type: String, enum: ["en", "mn", "jp", "cn"], default: "mn" },
});

module.exports = mongoose.model("Cat", catSchema);
