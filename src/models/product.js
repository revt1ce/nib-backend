const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const tabSchema = new mongoose.Schema({
	title: String,
	content: String,
});
const imageSchema = new mongoose.Schema({
	path: String,
	width: Number,
	height: Number,
	type: String,
});
const productSchema = new Schema({
	key: String,
	category: { type: mongoose.Types.ObjectId, ref: "Cat" },
	name: String,
	description: String,
	tabs: [tabSchema],
	image: imageSchema,
	author: String,
	language: { type: String, enum: ["en", "mn", "jp", "cn"], default: "mn" },
});

module.exports = mongoose.model("Product", productSchema);
