const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const imageSchema = new Schema({
	path: String,
	width: Number,
	height: Number,
	type: String,
});
const galleryItemSchema = new Schema({
	title: String,
	image: imageSchema,
});
const gallerySchema = new Schema({
	key: String,
	name: String,
	category: { type: mongoose.Types.ObjectId, ref: "Cat" },
	images: [galleryItemSchema],
	status: String,
	author: String,
	language: { type: String, enum: ["en", "mn", "jp", "cn"], default: "mn" },
});

module.exports = mongoose.model("Gallery", gallerySchema);
