const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const contentSchema = new Schema({
	path: String,
	width: Number,
	height: Number,
	type: String,
});

module.exports = mongoose.model("Content", contentSchema);
