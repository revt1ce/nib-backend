const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const imageSchema = new mongoose.Schema({
	path: String,
	width: Number,
	height: Number,
	type: String,
});
const postSchema = new Schema({
	key: String,
	catKey: String,
	category: {
		type: mongoose.Types.ObjectId,
		ref: "Cat",
	},
	title: String,
	description: String,
	body: String,
	images: [imageSchema],
	author: String,
	language: { type: String, enum: ["en", "mn", "jp", "cn"], default: "mn" },
});

module.exports = mongoose.model("Post", postSchema);
