const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const imageSchema = new mongoose.Schema({
	path: String,
	width: Number,
	height: Number,
	type: String,
});
const galleryItemSchema = new Schema({
	title: String,
	image: imageSchema,
});

module.exports = mongoose.model("GalleryItem", galleryItemSchema);
