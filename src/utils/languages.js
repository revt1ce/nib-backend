module.exports = {
	languages: [
		{
			country: "Монгол",
			slug: "mn",
		},
		{
			country: "English",
			slug: "en",
		},
		{
			country: "日本",
			slug: "jp",
		},
		{
			country: "中國",
			slug: "cn",
		},
	],
};
