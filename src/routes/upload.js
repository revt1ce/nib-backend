const express = require("express");
const router = express.Router();
const multer = require("multer");
const mongoose = require("mongoose");
const sharp = require("sharp");

function getFileExtension(filename) {
	const names = filename.split(".");
	if (names.length > 1) {
		return `.${names.pop()}`;
	}
	return "";
}

const imageUpload = multer({
	storage: multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, "public/uploads/images/");
		},
		filename: function (req, file, cb) {
			const idName = mongoose.Types.ObjectId();
			cb(null, `${idName}${getFileExtension(file.originalname)}`);
		},
	}),
});

// const videoUpload = multer({
// 	storage: multer.diskStorage({
// 		destination: function (req, file, cb) {
// 			cb(null, "./uploads/videos/");
// 		},
// 		filename: function (req, file, cb) {
// 			const idName = mongoose.Types.ObjectId();
// 			cb(null, `${idName}${getFileExtension(file.originalname)}`);
// 		},
// 	}),
// });

router.post("/images", imageUpload.array("files"), function (req, res) {
	const promises = req.files.map((file) => {
		let thumbName = file.filename.split(".");
		return new Promise((resolve) => {
			sharp(file.path)
				.resize(320, 240)
				.toFile(`public/uploads/images/${thumbName[0]}_small.${thumbName[1]}`)
				.then((data) => {
					console.log(data);
				})
				.catch((err) => {
					console.log(err);
				});
			resolve(file.path);
		});
	});

	Promise.all(promises).then((values) => {
		res.json(values);
	});
});

router.get("/", function (req, res) {
	res.json([]);
});

module.exports = router;
