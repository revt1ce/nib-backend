const { me, getUsers, getOneUser, register, login, updateUser, resetPassword, deleteUser } = require("./userResolver");
const {
	getCatsUser,
	getCats,
	getCatsKey,
	getOneCatKey,
	getOneCat,
	createCat,
	updateCat,
	orderCat,
	deleteCat,
} = require("./catResolver");
const { getLanguages } = require("./langResolver");
const { createContent, updateContent, deleteContent, getContents } = require("./contentResolver");
const {
	createPost,
	createPostLanguage,
	updatePost,
	deletePost,
	getPosts,
	getPostsKey,
	getOnePost,
	getPostsUser,
} = require("./postResolver");
const {
	createProduct,
	updateProduct,
	deleteProduct,
	getProducts,
	getProductsKey,
	getOneProduct,
	getProductsUser,
} = require("./productResolver");
const {
	createGallery,
	updateGallery,
	deleteGallery,
	getGalleries,
	getGalleriesKey,
	getOneGallery,
} = require("./galleryResolver");
module.exports = {
	Query: {
		getLanguages,
		//
		me,
		getUsers,
		getOneUser,
		//
		getCatsUser,
		getCats,
		getCatsKey,
		getOneCatKey,
		getOneCat,
		//
		getContents,
		//
		getPosts,
		getPostsKey,
		getOnePost,
		getPostsUser,
		//
		getGalleries,
		getGalleriesKey,
		getOneGallery,
		//
		getProducts,
		getProductsKey,
		getOneProduct,
		getProductsUser,
	},
	Mutation: {
		register,
		login,
		updateUser,
		resetPassword,
		deleteUser,
		//
		createCat,
		updateCat,
		orderCat,
		deleteCat,
		//
		createContent,
		updateContent,
		deleteContent,
		//
		createPost,
		createPostLanguage,
		updatePost,
		deletePost,
		//
		createGallery,
		updateGallery,
		deleteGallery,
		//
		createProduct,
		updateProduct,
		deleteProduct,
	},
};
