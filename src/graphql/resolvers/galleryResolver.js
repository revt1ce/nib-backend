const Gallery = require("../../models/gallery");
const GalleryItem = require("../../models/galleryItem");
const Cat = require("../../models/cat");
const { languages } = require("../../utils/languages");
const mongoose = require("mongoose");

module.exports = {
	createGallery: async (parent, args, context, info) => {
		const key = mongoose.Types.ObjectId();
		const catMn = await Cat.findOne({ key: args.catKey, language: "mn" });
		const galleryMn = await Gallery.create({
			key: key,
			name: args.name,
			category: catMn,
			author: args.author,
			language: "mn",
		});
		await Promise.all(
			languages.map(async (language) => {
				if (language.slug !== "mn") {
					const cat = await Cat.findOne({ key: args.catKey, language: language.slug });
					await Gallery.create({
						key,
						name: args.name,
						category: cat,
						author: args.author,
						language: language.slug,
					});
				}
			})
		);
		return Gallery.findById(galleryMn._id).populate("category");
	},
	updateGallery: async (parent, args, context, info) => {
		return Gallery.findByIdAndUpdate(gallery._id, args, { new: true }).populate("category");
	},
	deleteGallery: async (parent, args, context, info) => {
		await Promise.all(
			languages.map(async (language) => {
				await Gallery.findOneAndDelete({ key: args.key, language: language.slug });
			})
		);
		return true;
	},
	getGalleries: async (parent, args, context, info) => {
		let cat;
		if (args.catKey) {
			cat = await Cat.findOne({ key: args.catKey, language: args.language });
		}
		return Gallery.find(
			args.catKey ? { category: cat._id, language: args.language } : { language: args.language }
		).populate("category");
	},
	getGalleriesKey: async (parent, args, context, info) => {
		return Gallery.find({ key: args.key }).populate("category");
	},
	getOneGallery: async (parent, args, context, info) => {
		return Gallery.findById(args._id).populate("category");
	},
};
