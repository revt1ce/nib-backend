const Post = require("../../models/post");
const Cat = require("../../models/cat");
const { UserInputError } = require("apollo-server");
const mongoose = require("mongoose");

module.exports = {
	createPost: async (parent, args, context, info) => {
		const cat = await Cat.findOne({ key: args.catKey, language: args.language });
		const newPost = await Post.create({
			key: mongoose.Types.ObjectId(),
			title: args.title,
			description: args.description,
			body: args.body,
			catKey: args.catKey,
			category: cat,
			images: args.images,
			author: args.author,
			language: args.language,
		});
		return Post.findById(newPost._id).populate("category");
	},
	createPostLanguage: async (parent, args, context, info) => {
		const cat = await Cat.findOne({ language: args.language, key: args.catKey });
		const newPost = await Post.create({
			key: args.key,
			title: " ",
			description: " ",
			body: " ",
			catKey: args.catKey,
			category: cat,
			author: args.author,
			language: args.language,
		});
		return Post.findById(newPost._id).populate("category");
	},
	updatePost: async (parent, args, context, info) => {
		return Post.findByIdAndUpdate(args._id, args, { new: true }).populate("category");
	},
	deletePost: async (parent, args, context, info) => {
		await Post.findByIdAndDelete(args._id);
		return true;
	},
	getPosts: async (parent, args, context, info) => {
		let cat;
		if (args.catKey) {
			cat = await Cat.findOne({ key: args.catKey, language: args.language });
		}
		return Post.find(
			args.from
				? args.catKey
					? { language: args.language, catKey: args.catKey, _id: { $lt: args.from } }
					: { language: args.language, _id: { $lt: args.from } }
				: { language: args.language }
		)
			.limit(10)
			.sort({ _id: -1 })
			.populate("category");
	},
	getPostsKey: async (parent, args, context, info) => {
		return Post.find({ key: args.key }).populate("category");
	},
	getOnePost: async (parent, args, context, info) => {
		return Post.findById(args._id).populate("category");
	},
	getPostsUser: async (parent, args, context, info) => {
		return Post.find(
			args.catId
				? { language: context.language === "" ? "mn" : context.language, category: catId }
				: { language: context.language === "" ? "mn" : context.language }
		).populate("category");
	},
};
