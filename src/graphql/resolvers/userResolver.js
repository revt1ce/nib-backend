const { AuthenticationError, ForbiddenError, UserInputError, ApolloError } = require("apollo-server");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../../utils/constants");

const User = require("../../models/user");
const e = require("express");

module.exports = {
	me: async (parent, args, context, info) => {
		if (!context.user) {
			throw new AuthenticationError("Нэвтэрч орно уу!");
		}
		return User.findById(context.user);
	},
	register: async (parent, args, context, info) => {
		const user = await User.findOne({
			username: args.username,
		});
		if (user) {
			throw new UserInputError(args.username + " -ийм нэртэй хэрэглэгч бүртгэгдсэн байна.");
		} else {
			const password = bcrypt.hashSync(args.password, 12);
			return User.create({
				username: args.username,
				password: password,
				roles: args.roles,
			});
		}
	},
	login: async (parent, args, context, info) => {
		const user = await User.findOne({
			username: args.username,
		});
		if (user) {
			const match = await bcrypt.compare(args.password, user.password);
			if (match) {
				const token = jwt.sign(
					{
						user: user._id,
						role: user.role,
					},
					JWT_SECRET,
					{
						expiresIn: "2h",
					}
				);
				return { token, user };
			} else {
				throw new UserInputError("Нууц үг буруу байна!");
			}
		} else {
			throw new UserInputError(`${args.username}-ийм нэртэй хэрэглэгч олдсонгүй!`);
		}
	},
	updateUser: async (parent, args, context, info) => {
		const user = await User.findById(args._id);
		const exist = await User.findOne({ username: args.username });
		if (exist) {
			if (user.username !== exist.username) {
				throw new UserInputError(`${args.username}-ийм нэртэй хэрэглэгч байна!`);
			}
		}
		if (!user) {
			throw new UserInputError("Хэрэглэгч олдсонгүй!");
		}
		return User.findByIdAndUpdate(args._id, args, { new: true });
	},
	resetPassword: async (parent, args, context, info) => {
		const user = await User.findById(args._id);
		if (!user) {
			throw new UserInputError("Хэрэглэгч олдсонгүй!");
		}
		const password = bcrypt.hashSync(args.password, 12);
		return User.findByIdAndUpdate(args._id, { password: password }, { new: true });
	},
	deleteUser: async (parent, args, context, info) => {
		await User.findByIdAndDelete(args._id);
		return true;
	},
	getUsers: async (parent, args, context, info) => {
		return User.find();
	},
	getOneUser: async (parent, args, context, info) => {
		return User.findById(args._id);
	},
};
