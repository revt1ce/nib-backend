const Content = require("../../models/content");

module.exports = {
	createContent: async (parent, args, context, info) => {
		return Content.create(args);
	},
	updateContent: async (parent, args, context, info) => {
		return Content.findByIdAndUpdate(args._id, args, { new: true });
	},
	deleteContent: async (parent, args, context, info) => {
		await Content.findByIdAndDelete(args._id);
		return true;
	},
	getContents: async (parent, args, context, info) => {
		return Content.find(args.from ? { _id: { $lt: args.from }, type: args.type } : { type: args.type })
			.limit(8)
			.sort({ _id: -1 });
	},
};
