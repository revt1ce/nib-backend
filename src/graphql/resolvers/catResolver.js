const { AuthenticationError, ForbiddenError, UserInputError, ApolloError } = require("apollo-server");
const mongoose = require("mongoose");
const { languages } = require("../../utils/languages");

const Cat = require("../../models/cat");

module.exports = {
	createCat: async (_, args, context, info) => {
		const { parentKey } = args;

		let parent = null;
		if (parentKey) {
			parent = await Cat.findOne({ key: parentKey, language: "mn" });
			if (!parent) {
				throw new UserInputError("Алдаа гарлаа [parent]");
			}
		}

		const mnChildren = await Cat.create({
			key: mongoose.Types.ObjectId(),
			name: args.name,
			slug: args.slug,
			parentKey: parentKey,
			layout: args.layout,
			image: args.image,
			language: "mn",
		});

		if (parent) {
			await parent.childrenCats.push(mnChildren);
			await parent.save();
		}

		await Promise.all(
			languages.map(async (language) => {
				if (language.slug !== "mn") {
					const children = await Cat.create({
						key: mnChildren.key,
						name: " ",
						slug: mnChildren.slug,
						parentKey: mnChildren.parentKey,
						layout: mnChildren.layout,
						image: mnChildren.image,
						language: language.slug,
					});

					if (parentKey) {
						const parentCat = await Cat.findOne({ language: language.slug, key: parentKey });
						parentCat.childrenCats.push(children);
						await parentCat.save();
					}
				}
			})
		);

		return mnChildren;
	},
	updateCat: async (parent, args, context, info) => {
		const cat = await Cat.findByIdAndUpdate(args._id, args, { new: true });
		await Promise.all(
			languages.map(async (language) => {
				if (language !== "mn") {
					await Cat.findOneAndUpdate(
						{ language: language.slug, key: cat.key },
						{ image: cat.image, slug: cat.slug }
					);
				}
			})
		);
		return cat;
	},
	orderCat: async (parent, args) => {
		await Promise.all(
			args.params.map((catId, index) => {
				return Cat.findByIdAndUpdate(catId, { ordering: index });
			})
		);
		return true;
	},
	deleteCat: async (parent, args, context, info) => {
		await Promise.all(
			languages.map(async (language) => {
				await Cat.findOneAndDelete({ key: args.key, language: language.slug });
			})
		);
		return true;
	},
	getCats: async (parent, args, context, info) => {
		return Cat.find({ language: args.language, parentKey: args.parentKey ? args.parentKey : null }).sort({
			ordering: 1,
		});
	},
	getCatsKey: async (parent, args, context, info) => {
		return Cat.find({ key: args.key });
	},
	getOneCatKey: async (parent, args, context, info) => {
		return Cat.findOne({ key: args.key, language: args.language })
			.populate("childrenCats")
			.populate({
				path: "childrenCats",
				populate: {
					path: "childrenCats",
				},
			});
	},
	getOneCat: async (parent, args, context, info) => {
		return Cat.findById(args._id);
	},
	getCatsUser: async (parent, args, context, info) => {
		return Cat.find({
			language: context.language === "" ? "mn" : context.language,
			parentKey: null,
		})
			.sort({
				ordering: 1,
			})
			.populate({
				path: "childrenCats",
				populate: {
					path: "childrenCats",
				},
			});
	},
};
