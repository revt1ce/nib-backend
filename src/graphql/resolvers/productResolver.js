const Product = require("../../models/product");
const Cat = require("../../models/cat");
const mongoose = require("mongoose");
const { languages } = require("../../utils/languages");

module.exports = {
	createProduct: async (parent, args, context, info) => {
		const key = mongoose.Types.ObjectId();
		const catMn = await Cat.findOne({ key: args.catKey, language: "mn" });
		const productMn = await Product.create({
			key: key,
			category: catMn,
			name: args.name,
			description: args.description,
			image: args.image,
			tabs: args.tabs,
			author: args.author,
			language: "mn",
		});
		await Promise.all(
			languages.map(async (language) => {
				if (language.slug !== "mn") {
					const cat = await Cat.findOne({ key: args.catKey, language: language.slug });
					await Product.create({
						key: key,
						category: cat,
						name: " ",
						description: " ",
						image: args.image,
						author: productMn.author,
						language: language.slug,
					});
				}
			})
		);
		return Product.findById(productMn._id).populate("category");
	},
	updateProduct: async (parent, args, context, info) => {
		if (args.catKey) {
			const product = await Product.findById(args._id);
			const newCat = await Cat.findOne({ key: args.catKey, language: product.language });
			const newProduct = await Product.findByIdAndUpdate(product._id, {
				category: newCat,
				name: args.name,
				description: args.description,
				image: args.image,
				tabs: args.tabs,
			}).populate("category");

			languages.map(async (language) => {
				if (languages.slug !== newProduct.language) {
					const otherCat = await Cat.findOne({ key: newCat.key, language: language.slug });
					await Product.findOneAndUpdate(
						{ key: newProduct.key, language: language.slug },
						{
							category: otherCat,
						}
					);
				}
			});
			return newProduct;
		} else {
			return Product.findByIdAndUpdate(args._id, args, { new: true }).populate("category");
		}
	},
	deleteProduct: async (parent, args, context, info) => {
		await Promise.all(
			languages.map(async (language) => {
				await Product.findOneAndDelete({ key: args.key, language: language.slug });
			})
		);
		return true;
	},
	getProducts: async (parent, args, context, info) => {
		let cat;
		if (args.catKey) {
			cat = await Cat.findOne({ key: args.catKey, language: args.language });
		}
		return Product.find(
			args.from
				? args.catKey
					? { category: cat._id, language: args.language, _id: { $lt: args.from } }
					: { language: args.language, _id: { $lt: args.from } }
				: { language: args.language }
		)
			.limit(10)
			.sort({ _id: -1 })
			.populate("category");
	},
	getProductsKey: async (parent, args, context, info) => {
		return Product.find({ key: args.key }).populate("category");
	},
	getOneProduct: async (parent, args, context, info) => {
		return Product.findById(args._id).populate("category");
	},
	getProductsUser: async (parent, args, context, info) => {
		return Product.find(
			args.catId
				? { category: args.catId, language: context.language === "" ? "mn" : context.language }
				: { language: context.language === "" ? "mn" : context.language }
		).populate("category");
	},
};
