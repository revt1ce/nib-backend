const { languages } = require("../../utils/languages");

module.exports = {
	getLanguages: async (parent, args, context, info) => {
		return languages;
	},
};
