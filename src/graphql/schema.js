const { gql } = require("apollo-server-express");

module.exports = gql`
	type Language {
		country: String
		slug: String
	}
	type Auth {
		user: User
		token: String
	}
	type Content {
		_id: ID!
		path: String
		width: Int
		height: Int
		type: String
	}
	type Tab {
		title: String
		content: String
	}
	type GalleryItem {
		title: String
		image: Content
	}
	type User {
		_id: ID!
		username: String
		roles: [String]
	}
	type Gallery {
		_id: ID!
		key: String
		category: Cat
		name: String
		images: [GalleryItem]
		author: String
		language: String
	}
	type Product {
		_id: ID!
		key: String
		category: Cat
		name: String
		description: String
		image: Content
		tabs: [Tab]
		author: String
		language: String
	}
	type Post {
		_id: ID!
		key: String
		catKey: String
		category: Cat
		title: String
		description: String
		body: String
		images: [Content]
		author: String
		language: String
	}
	type Cat {
		_id: ID!
		key: String
		name: String!
		slug: String!
		parentKey: String
		childrenCats: [Cat]
		layout: Int
		image: Content
		language: String
	}
	input ContentInput {
		_id: String!
		path: String
		width: Int
		height: Int
		type: String
	}
	input TabInput {
		title: String
		content: String
	}
	input GalleryItemInput {
		title: String
		image: ContentInput
	}

	type Query {
		getLanguages: [Language]
		#
		me: User
		getUsers: [User]
		getOneUser(_id: ID!): User
		#
		getCats(language: String, parentKey: String): [Cat]
		getCatsKey(key: String): [Cat]
		getOneCatKey(key: String, language: String): Cat
		getOneCat(_id: ID!): Cat
		#
		getContents(from: String, type: String): [Content]
		#
		getPosts(from: String, catKey: String, language: String): [Post]
		getPostsKey(key: String): [Post]
		getOnePost(_id: ID!): Post
		#
		getGalleries(catKey: String, language: String): [Gallery]
		getGalleriesKey(key: String): [Gallery]
		getOneGallery(_id: ID!): Gallery
		#
		getProducts(from: String, catKey: String, language: String): [Product]
		getProductsKey(key: String): [Product]
		getOneProduct(_id: ID!): Product
		getProductsUser(catId: String): [Product]
		#
		#
		getPostsUser(catId: String): [Post]
		getCatsUser: [Cat]
	}
	type Mutation {
		register(username: String, password: String, roles: [String]): User
		login(username: String, password: String): Auth
		updateUser(_id: ID!, username: String, roles: [String]): User
		resetPassword(_id: ID!, password: String): User
		deleteUser(_id: ID!): Boolean
		#
		createCat(name: String, slug: String, parentKey: String, layout: Int, image: ContentInput): Cat
		updateCat(_id: ID!, name: String, slug: String, image: ContentInput): Cat
		orderCat(params: [String]): Boolean
		deleteCat(key: String): Boolean
		#
		createContent(path: String, type: String): Content
		updateContent(_id: ID!, path: String, width: Int, height: Int): Content
		deleteContent(_id: ID!): Boolean
		#
		createPost(
			key: String
			title: String
			description: String
			body: String
			catKey: String
			images: [ContentInput]
			author: String
			language: String
		): Post
		createPostLanguage(key: String, catKey: String, language: String, author: String): Post
		updatePost(
			_id: ID!
			title: String
			description: String
			body: String
			catKey: String
			images: [ContentInput]
		): Post
		deletePost(_id: ID!): Boolean
		#
		createGallery(name: String, catKey: String, author: String): Gallery
		updateGallery(name: String, images: [GalleryItemInput]): Gallery
		deleteGallery(key: String): Boolean
		#
		createProduct(
			catKey: String
			name: String
			description: String
			image: ContentInput
			tabs: [TabInput]
			author: String
		): Product
		updateProduct(
			_id: ID!
			catKey: String
			name: String
			description: String
			image: ContentInput
			tabs: [TabInput]
		): Product
		deleteProduct(key: String): Boolean
	}
`;
